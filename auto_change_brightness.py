#!/usr/bin/env python
# -*- coding: utf-8 -*-

from PIL import Image, ImageEnhance
import cv2
import os

def brightness():
	BASE_DIR = os.path.dirname(os.path.abspath(__file__))
	image_dir = os.path.join(BASE_DIR, "face-training-data")
	brightness_image_dir = os.path.join(BASE_DIR, "brightness")

	if not os.path.isdir(brightness_image_dir):
		os.mkdir(os.path.join(BASE_DIR, "brightness"))

	for root, dirs, files in os.walk(image_dir):
		if os.path.basename(root).startswith('s') and not os.path.isdir(os.path.join(brightness_image_dir, os.path.basename(root))):
			os.mkdir( os.path.join(brightness_image_dir, os.path.basename(root)) )

		i = ( (len(files)) * 4 ) + 1
		for file in files:	
			path = os.path.join(root, file)
			imag_format = os.path.splitext(os.path.basename(file))[1]
			image_name = str(i) + imag_format

			image = Image.open(path)

			try:
				if hasattr(image, '_getexif'):
					orientation = 0x0112
					exif = image._getexif()
					if exif is not None:
						orientation = exif[orientation]
						rotations = {
							3: Image.ROTATE_180,
							6: Image.ROTATE_270,
							8: Image.ROTATE_90
						}
						if orientation in rotations:
							image = image.transpose(rotations[orientation])
			except:
				pass

			img_high_brightness = ImageEnhance.Brightness(image).enhance(2.0)
			img_low_brightness = ImageEnhance.Brightness(image).enhance(0.3)

			directory = os.path.join(brightness_image_dir, os.path.join(brightness_image_dir, os.path.basename(root)))
			img_high_brightness.save(os.path.join(directory, image_name))
			i = i + 1
			image_name = str(i) + imag_format
			img_low_brightness.save(os.path.join(directory, image_name))
			i = i + 1

if __name__ == '__main__':
	brightness()