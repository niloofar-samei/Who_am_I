#!/usr/bin/env python
# -*- coding: utf-8 -*-

import cv2
import os

def flip():
	BASE_DIR = os.path.dirname(os.path.abspath(__file__))
	image_dir = os.path.join(BASE_DIR, "face-training-data")
	flip_image_dir = os.path.join(BASE_DIR, "flip")

	if not os.path.isdir(flip_image_dir):
		os.mkdir(os.path.join(BASE_DIR, "flip"))

	for root, dirs, files in os.walk(image_dir):
		if os.path.basename(root).startswith('s') and not os.path.isdir(os.path.join(flip_image_dir, os.path.basename(root))):
			os.mkdir( os.path.join(flip_image_dir, os.path.basename(root)) )

		i = (len(files)) + 1
		for file in files:	
			path = os.path.join(root, file)
			imag_format = os.path.splitext(os.path.basename(file))[1]
			image_name = str(i) + imag_format
			image = cv2.imread(path)
			flip_image = cv2.flip( image, 1 )
			directory = os.path.join(flip_image_dir, os.path.basename(root))
			cv2.imwrite(os.path.join(directory, image_name), flip_image)
			i = i + 1

if __name__ == '__main__':
	flip()