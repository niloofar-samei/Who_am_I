#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import cv2
import os
import numpy as np
import auto_flip_photos, auto_change_contrast, auto_change_brightness, auto_draw_lines
from pygame import mixer

face_recognizer = cv2.face.LBPHFaceRecognizer_create()
not_detected_images = []

def get_name_list(*args):
	with open('name_list.txt', 'w') as f:
		f.write('' + '\n')
		for arg in args:
			f.write(arg + '\n')

def detect_face(img, *args):
	gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
	face_cascade = cv2.CascadeClassifier("/home/niloofar/git/venv/lib/python3.6/site-packages/cv2/data/haarcascade_frontalface_default.xml")
	faces = face_cascade.detectMultiScale(gray, scaleFactor=1.2, minNeighbors=5)

	if (len(faces) == 0):
		for arg in args:
			not_detected_images.append(arg)

	return faces, gray

def prepare_training_data(data_folder_path):
	if os.path.isdir(data_folder_path):
		BASE_DIR = os.path.dirname(os.path.abspath(__file__))
		image_dir = os.path.join(BASE_DIR, data_folder_path)
		faces_list = []
		labels_list = []

		for root, dirs, files in os.walk(image_dir):
			for file in files:
				path = os.path.join(root, file)
				label = int(os.path.basename(root).replace("s", ""))
				image = cv2.imread(path)
				#cv2.imshow("Training on image...", image)
				cv2.waitKey(100)			 
				face, rect = detect_face(image, (os.path.join(os.path.join((root.split('/')[6]), os.path.basename(root)), file)))
				if face is not None:
					faces_list.append(rect)
					labels_list.append(label)

		auto_flip_photos.flip()
		flip_image_dir = os.path.join(BASE_DIR, "flip")
		for root, dirs, files in os.walk(flip_image_dir):
			for file in files:
				path = os.path.join(root, file)
				label = int(os.path.basename(root).replace("s", ""))
				image = cv2.imread(path)
				#cv2.imshow("Training on image...", image)
				cv2.waitKey(100)
				face, rect = detect_face(image, (os.path.join(os.path.join((root.split('/')[6]), os.path.basename(root)), file)))
				if face is not None:
					faces_list.append(rect)
					labels_list.append(label)

		auto_change_contrast.contrast()
		contrast_image_dir = os.path.join(BASE_DIR, "contrast")
		for root, dirs, files in os.walk(contrast_image_dir):
			for file in files:
				path = os.path.join(root, file)
				label = int(os.path.basename(root).replace("s", ""))
				image = cv2.imread(path)
				#cv2.imshow("Training on image...", image)
				cv2.waitKey(100)
				face, rect = detect_face(image, (os.path.join(os.path.join((root.split('/')[6]), os.path.basename(root)), file)))
				if face is not None:
					faces_list.append(rect)
					labels_list.append(label)

		auto_change_brightness.brightness()
		brightness_image_dir = os.path.join(BASE_DIR, "brightness")
		for root, dirs, files in os.walk(brightness_image_dir):
			for file in files:
				path = os.path.join(root, file)
				label = int(os.path.basename(root).replace("s", ""))
				image = cv2.imread(path)
				#cv2.imshow("Training on image...", image)
				cv2.waitKey(100)
				face, rect = detect_face(image, (os.path.join(os.path.join((root.split('/')[6]), os.path.basename(root)), file)))
				if face is not None:
					faces_list.append(rect)
					labels_list.append(label)

		auto_draw_lines.lines()
		lines_image_dir = os.path.join(BASE_DIR, "lines")
		for root, dirs, files in os.walk(lines_image_dir):
			for file in files:
				path = os.path.join(root, file)
				label = int(os.path.basename(root).replace("s", ""))
				image = cv2.imread(path)
				#cv2.imshow("Training on image...", image)
				cv2.waitKey(100)
				face, rect = detect_face(image, (os.path.join(os.path.join((root.split('/')[6]), os.path.basename(root)), file)))
				if face is not None:
					faces_list.append(rect)
					labels_list.append(label)

		cv2.destroyAllWindows()
		cv2.waitKey(1)
		cv2.destroyAllWindows()

		faces = faces_list
		labels = labels_list
		face_recognizer.train(faces, np.array(labels))
		face_recognizer.save("face-trainner.yml")

		thefile = open('not_detected_images.txt', 'w')
		for item in not_detected_images:
			thefile.write("%s\n" % item)

def draw_rectangle(frame, x, y, w, h):
	cv2.rectangle(frame, (x, y), (x+w, y+h), (225, 179, 255), 2)
 
def draw_text(frame, label_text, confidence, x, y):
	cv2.putText(frame, (label_text + confidence), (x,y), cv2.FONT_HERSHEY_SIMPLEX, 1, (255,255,255), 2)

def play_audio(person, specific_name):
	mixer.init()
	if person.count(specific_name) >= 6:
		try:
			mixer.music.load('welcome.mp3')
			mixer.music.play()
		except:
			pass
			
		try:
			os.remove("out.avi")
		except:
			pass
	else:
		try:
			mixer.music.load('alarm_invasion.mp3')
			mixer.music.play()
		except:
			pass
			
def who_am_i(*args):
	try:
		with open('name_list.txt', 'r') as f:
			name_list = [line.rstrip('\n') for line in f]
	except:
		pass

	try:
		face_recognizer.read("face-trainner.yml")
	except:
		pass

	person = []
	cap = cv2.VideoCapture(0)
	if args:
		out = cv2.VideoWriter('out.avi',cv2.VideoWriter_fourcc('M','J','P','G'), 10, (int(cap.get(3)),int(cap.get(4))))

	while(True):
		ret, frame = cap.read()
		frame = cv2.flip(frame, 1)
		faces, gray = detect_face(frame)

		for (x, y, w, h) in faces:
			draw_rectangle(frame, x, y, w, h)
			label, confidence= face_recognizer.predict(gray[y:y+h,x:x+w])

			if (confidence < 100 and confidence <= 60):
				label_text = name_list[label]
				confidence = " ({0}%)".format(round(100 - confidence))
			else:
				label_text = "unknown"
				confidence = ""

			draw_text(frame, label_text, confidence, (x+5), (y-5))
			if args:
				out.write(frame)
				if len(person) < 11:
					person.append(label_text)
				if len(person) == 11:
					person.append('end')
					for arg in args:
						specific_name = arg
					play_audio(person, specific_name)

		cv2.imshow('frame', frame)
		if cv2.waitKey(1) & 0xFF == ord('q'):
			break

	cap.release()
	if args:
		out.release()
	cv2.destroyAllWindows()

if __name__ == '__main__':
	get_name_list(names)
	prepare_training_data(data_folder_path)
	who_am_i(specific_name)