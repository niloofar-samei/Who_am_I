#!/usr/bin/env python
# -*- coding: utf-8 -*-

from PIL import Image, ImageDraw
import os

def lines():
	BASE_DIR = os.path.dirname(os.path.abspath(__file__))
	image_dir = os.path.join(BASE_DIR, "face-training-data")
	lines_image_dir = os.path.join(BASE_DIR, "lines")

	if not os.path.isdir(lines_image_dir):
		os.mkdir(os.path.join(BASE_DIR, "lines"))

	for root, dirs, files in os.walk(image_dir):
		if os.path.basename(root).startswith('s') and not os.path.isdir(os.path.join(lines_image_dir, os.path.basename(root))):
			os.mkdir( os.path.join(lines_image_dir, os.path.basename(root)) )

		i = ( (len(files)) * 6 ) + 1
		for file in files:	
			path = os.path.join(root, file)
			imag_format = os.path.splitext(os.path.basename(file))[1]
			image_name = str(i) + imag_format

			image = Image.open(path)

			try:
				if hasattr(image, '_getexif'):
					orientation = 0x0112
					exif = image._getexif()
					if exif is not None:
						orientation = exif[orientation]
						rotations = {
							3: Image.ROTATE_180,
							6: Image.ROTATE_270,
							8: Image.ROTATE_90
						}
						if orientation in rotations:
							image = image.transpose(rotations[orientation])
			except:
				pass

			draw = ImageDraw.Draw(image)
			y_start = 0
			y_end = image.height
			step_size = int(image.width / 10)

			for x in range(0, image.width, step_size):
				line = ((x, y_start), (x, y_end))
				draw.line(line, fill=128)

			x_start = 0
			x_end = image.width

			for y in range(0, image.height, step_size):
				line = ((x_start, y), (x_end, y))
				draw.line(line, fill=128)

			del draw

			directory = os.path.join(lines_image_dir, os.path.join(lines_image_dir, os.path.basename(root)))
			image.save(os.path.join(directory, image_name))
			i = i + 1

if __name__ == '__main__':
	lines()